<?php
/**
 * The template for displaying all archive pages.
 */

get_header();

$paged = get_query_var('paged') ? get_query_var('paged') : 1;

query_posts(array('post_type' => 'blog', 'posts_per_page' => -1, 'paged' => $paged));

?>

	<div id="primary" class="work content-area small-12 columns">
		<main id="main" class="site-main" role="main">

		<header class="portfolio-header">
          <h2 class="entry-big">Blog</h2>
        </header>

			<?php if (have_posts()) : ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('blog-entry'); ?>>

						<div class="entry-content fullpage">

							<div class="entry-header">
								<h1 class="entry-title"><a href="<?php echo get_permalink(); ?>"><?php the_title( ); ?></a></h1>

								<small class="entry-posted">Posted on <?php the_time('l, F jS, Y') ?></small>
							</div>

							<div class="inner-content">
								<?php the_excerpt(); ?>
							</div>

						</div><!-- .entry-content -->

						<footer class="entry-footer">
							<a class="read-more" href="<?php echo get_permalink(); ?>">Read more</a>
						</footer><!-- .entry-footer -->

					</article><!-- #post-## -->

				<?php endwhile; // End of the loop. ?>

			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
