<?php
/**
 * PQA theme functions and definitions
 *
 * @package pqa
 */

/*--------------------------------------------------
 * Content Width
 *--------------------------------------------------*/
  function pqa_content_width() {
    $GLOBALS['content_width'] = apply_filters('pqa_content_width', 640);
  }
  add_action('after_setup_theme', 'pqa_content_width', 0);

/*--------------------------------------------------
* Theme Setup
*--------------------------------------------------*/

if (! function_exists('pqa_setup')) :

function pqa_setup()
{
  /*--------------------------------------------------
   * Theme Support
   *--------------------------------------------------*/

  add_theme_support('title-tag');
  add_theme_support('post-thumbnails');

  /*--------------------------------------------------
   * Menus
   *--------------------------------------------------*/
  register_nav_menus(array(
      'primary' => esc_html__('Primary Menu', 'pqa'),
  ));
}
endif; // pqa_setup
add_action('after_setup_theme', 'pqa_setup');



/*--------------------------------------------------
 * Image sizes
 *--------------------------------------------------*/

add_image_size('work', 300, 200, true);
add_image_size('work_gallery', 200, 200, true);
add_image_size('work_normal', 400, 400, true);


/*--------------------------------------------------
 * Widgets
 *--------------------------------------------------*/
function pqa_widgets_init()
{
  register_sidebar(array(
    'name'          => esc_html__('Sidebar', 'pqa'),
    'id'            => 'sidebar-1',
    'description'   => '',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ));
}
add_action('widgets_init', 'pqa_widgets_init');

/*--------------------------------------------------
 * Fonts
 *--------------------------------------------------*/

function pqa_add_fonts()
{
    echo "<link href='https://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>";
}
add_action('wp_head', 'pqa_add_fonts');

/*--------------------------------------------------
 * Scripts and styles
 *--------------------------------------------------*/
function pqa_scripts()
{
  // styles
  wp_enqueue_style('underscores-css', get_template_directory_uri().'/css/underscores.css');
  wp_enqueue_style('foundation-css', get_template_directory_uri().'/css/foundation.css');
  wp_enqueue_style('pqa-style', get_stylesheet_uri());
  wp_enqueue_style('animate-css', get_template_directory_uri().'/css/animate.css');
  wp_enqueue_style('gist-css', get_template_directory_uri().'/css/gist.css');
  wp_enqueue_style('lightbox-css', get_template_directory_uri().'/css/lightbox.css');
  wp_enqueue_style('foundation-icons', get_template_directory_uri().'/fonts/foundation-icons.css');

  //scripts
  wp_enqueue_script('pqa-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true);
  wp_enqueue_script('pqa-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true);
  wp_enqueue_script('foundation-js', get_template_directory_uri().'/js/foundation.min.js', array('jquery'));
  wp_enqueue_script('misc-js', get_template_directory_uri().'/js/misc.js', array('jquery'));
  wp_enqueue_script('lightbox-js', get_template_directory_uri().'/js/lightbox.min.js', array('jquery'), false, true);

  if (is_singular() && comments_open() && get_option('thread_comments')) {
      wp_enqueue_script('comment-reply');
  }
}
add_action('wp_enqueue_scripts', 'pqa_scripts');


/*--------------------------------------------------
 * Includes
 *--------------------------------------------------*/

include get_template_directory() . '/inc/template-tags.php';
include get_template_directory() . '/inc/extras.php';
include get_template_directory() . '/inc/custom-post-types.php';

/*--------------------------------------------------
 * Misc
 *--------------------------------------------------*/

// make tags associated with custom post type work
function wpa_cpt_tags($query)
{
  if ($query->is_tag() && $query->is_main_query()) {
    $query->set('post_type', array( 'work' ));
  }
}
add_action('pre_get_posts', 'wpa_cpt_tags');

// add more buttons to the html editor
function pqa_add_quicktags()
{
  if (wp_script_is('quicktags')) { ?>
    <script type="text/javascript">
      QTags.addButton( 'pqa_highlight', 'highlight', '<span class="inline">', '</span>', 'Highlight', 'Highlight tag', 1 );
    </script>
  <?php }
}
add_action('admin_print_footer_scripts', 'pqa_add_quicktags');

// move YOAST metabox to last place in admin
add_filter('wpseo_metabox_prio', function () {
    return 'low';
});
