<?php
/**
 * The template for displaying the header
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

<link href="https://fonts.googleapis.com/css?family=Kanit:100,100i,200,300,300i,400,500,500i,700,700i" rel="stylesheet">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div class="top-bar container">
	<div class="row">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'pqa' ); ?></a>

		<header id="masthead" class="site-header small-12 columns" role="banner">
			<div class="row">
				<div class="site-branding small-4 columns">
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				</div><!-- .site-branding -->


				<nav id="site-navigation" class="main-navigation small-8 columns" role="navigation">
					<button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
						<img src="<?php echo get_template_directory_uri() . '/images/lines.svg'; ?>"/>
					</button>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
				</nav><!-- #site-navigation -->
			</div>
		</header><!-- #masthead -->

	</div>
</div>

<div class="container">
	<div class="row">

		<div id="page" class="hfeed site">
			<div id="content" class="site-content">
