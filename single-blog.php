<?php
/**
 * The template for displaying the single-blog templates.
 */
get_header(); ?>

	<div id="primary" class="content-area medium-12 columns">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="entry-content fullpage">

					<div class="entry-header">
						<h1 class="entry-title"><?php the_title( ); ?></h1>
						<small class="entry-posted">Posted on <?php the_time('l, F jS, Y') ?></small>
					</div>

					<div class="inner-content">
						<?php echo apply_filters( 'the_content', $post->post_content ); ?>
					</div>

				</div><!-- .entry-content -->

				<footer class="entry-footer">
					<?php pqa_entry_footer(); ?>
				</footer><!-- .entry-footer -->

			</article><!-- #post-## -->

			<?php the_post_navigation(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
