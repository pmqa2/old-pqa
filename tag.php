<?php
/**
 * The template for displaying the tags.
 */

get_header();

$paged = get_query_var('paged') ? get_query_var('paged') : 1;

$queried_object = get_queried_object();
// WP_Query arguments
$args = array(
	'post_type' => 'work',
	'tag_id' => $queried_object->term_id
);

// The Query
$query = new WP_Query( $args );

?>

	<div id="primary" class="work content-area small-12 columns">
		<main id="main" class="site-main" role="main">

			<header class="portfolio-header">
	      <h2 class="entry-big"><?php echo $queried_object->name; ?> Portfolio</h2>
	    </header>

			<?php if ($query->have_posts()) : ?>

				<?php while ( $query->have_posts() ) : $query->the_post(); ?>

					<?php get_template_part( 'parts/content', 'archive' ); ?>

				<?php endwhile; // End of the loop. ?>

			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
