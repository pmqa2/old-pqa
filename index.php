<?php
/**
 * The template for displaying the index page.
 */

get_header();

query_posts(array('category_name' => 'homepage'));

?>

		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<div class="wrapper-centered">

						<div class="inner-centered">

							<?php switch (get_field('type')) {
								case 'intro-image':
									get_template_part( 'parts/content', 'index' );
									break;

								case 'text-only':
									get_template_part( 'parts/content', 'index-text' );
									break;

								default:
									break;
								}
							?>

						</div>

					</div>

				<?php endwhile; ?>

				<?php the_posts_navigation(); ?>

			<?php else : ?>

				<?php get_template_part( 'parts/content', 'none' ); ?>

			<?php endif; ?>

			</main><!-- #main -->
		</div><!-- #primary -->

<?php get_footer(); ?>
