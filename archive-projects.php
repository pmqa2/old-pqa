<?php
/**
 * The template for displaying all archive pages.
 */

get_header();

$paged = get_query_var('paged') ? get_query_var('paged') : 1;

query_posts(array('post_type' => 'projects', 'posts_per_page' => -1, 'paged' => $paged));

?>

	<div id="primary" class="work content-area small-12 columns">
		<main id="main" class="site-main" role="main">

		<header class="portfolio-header">
          <h2 class="entry-big">Projects</h2>
		  <p>Some of my most recent professional work is here. Check out my <a href="#">personal projects</a> page too.</p>
        </header>

			<?php if (have_posts()) : ?>

				<?php while (have_posts()) : the_post(); ?>

					<?php get_template_part('parts/content', 'archive'); ?>

				<?php endwhile; // End of the loop.?>

			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
