<?php
/**
 * The template for displaying the singl-work templates.
 */

get_header();

?>

	<div id="primary" class="work content-area small-12 columns">
		<main id="main" class="site-main" role="main">

		<header class="portfolio-header">
          <h2 class="entry-big">Work</h2>
        </header>

			<?php if (have_posts()) : ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'parts/content', 'archive' ); ?>

				<?php endwhile; // End of the loop. ?>

			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
