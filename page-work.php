<?php
/**
 * The template for displaying the page-work template.
 */

get_header();

$args = array('post_type' => 'post', 'category_name' => 'work');
$work = new WP_QUERY($args);
?>

	<div id="primary" class="work content-area small-12 columns">
		<main id="main" class="site-main" role="main">

		<h2 class="entry-big"><?php the_title(); ?></h2>
		<p><?php the_content( ); ?></p>

			<?php if ($work->have_posts()) : ?>

				<?php while ( $work->have_posts() ) : $work->the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<div class="entry-content">

							<?php $gallery = get_field('gallery'); ?>
							<?php $featured = get_field('featured'); ?>
							<?php $job_role = get_field('job_role'); ?>

							<div class="work-images">

							<?php if ($featured) : ?>
								<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
							<?php else : ?>
								<?php $feat_image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'work_normal' ); ?>
								<?php $feat_image = $feat_image_array[0]; ?>
							<?php endif; ?>

							<a href="<?php echo $feat_image; ?>" data-lightbox='<?php echo $post->post_name; ?>'><img src="<?php echo $feat_image; ?>"></a>

							<?php if ($gallery) : ?>

								<div class="work-images-gallery">

									<?php foreach ($gallery as $g) : ?>
										<div class="small-6 columns"><a href="<?php echo $g['url']; ?>" data-lightbox='<?php echo $post->post_name; ?>'><img src="<?php echo $g['sizes']['work_gallery']; ?>" alt=""></a></div>
									<?php endforeach; ?>

								</div>

							<?php endif; ?>

							</div>

							<div class="work-desc">
								<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
								<div class="job-meta">
										<?php if ($job_role) :?>
											<span class="job-title"><?php echo $job_role; ?></span>
										<?php endif; ?>
									</span>
									<?php /* <span class="job-url"><i class="fi-link"></i></span>*/ ?>
								</div>
								<?php the_content( ); ?>

								<div class="work-tags">
									<?php the_tags('',''); ?>
								</div>
							</div>

						</div><!-- .entry-content -->

						<footer class="entry-footer">
							<?php pqa_entry_footer(); ?>
						</footer><!-- .entry-footer -->

					</article><!-- #post-## -->

				<?php endwhile; // End of the loop. ?>

			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
