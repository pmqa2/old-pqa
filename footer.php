<?php
/**
 * The template for displaying the footer.
 */

?>

	</div><!-- #content -->

	</div>
</div>

<div class="container">
	<div class="row">
		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="site-info">
				<span>Designed and developed by pqa.me. That's me!</span>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- .row -->
</div><!-- .container -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
