<?php
/**
 * Custom functions that act independently of the theme templates.
 *
*/
function pqa_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter( 'body_class', 'pqa_body_classes' );
