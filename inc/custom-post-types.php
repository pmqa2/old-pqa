<?php

/* EVENTOS CUSTOM POSTTYPE +++++++++++++++ */
function cpt_work() {
    $cpt_name = 'Work';
    $cpt_slug = 'work';
    $cpt_name_singular = 'Work';
    $labels = array(
        'name'               => _x( $cpt_name, 'post type general name' ),
        'singular_name'      => _x( $cpt_name_singular, 'post type singular name' ),
        'add_new'            => _x( 'Adicionar novo(a)', $cpt_name_singular ),
        'add_new_item'       => __( 'Adicionar novo(a)' ),
        'edit_item'          => __( 'Editar '.$cpt_name_singular ),
        'new_item'           => __( 'Novo(a) '.$cpt_name_singular ),
        'all_items'          => __( 'Ver todos(as)' ),
        'view_item'          => __( 'Ver' ),
        'search_items'       => __( 'Procurar' ),
        'not_found'          => __( 'Nada encontrado' ),
        'not_found_in_trash' => __( 'Nada encontrado no lixo' ),
        'parent_item_colon'  => '',
        'menu_name'          => $cpt_name
    );
    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_nav_menus'     => true,
        'show_in_admin_bar'     => true,
        'query_var'             => true,
        'capability_type'       => 'post',
        'has_archive'           => true,
        'hierarchical'          => false,
        'can_export'            => true,
        'exclude_from_search'   => false,
		 		'taxonomies'            => array('post_tag'),
        'description'           => 'Adiciona entradas para '.$cpt_name,
        'rewrite'               => array('slug' => $cpt_slug),
        'menu_position'         => null,
        'supports'              => array( 'title','editor','thumbnail' ),
    );
    register_post_type( $cpt_slug, $args );
}
add_action( 'init', 'cpt_work' );

/* EVENTOS CUSTOM POSTTYPE +++++++++++++++ */
function cpt_projects() {
    $cpt_name = 'Projects';
    $cpt_slug = 'projects';
    $cpt_name_singular = 'Project';
    $labels = array(
        'name'               => _x( $cpt_name, 'post type general name' ),
        'singular_name'      => _x( $cpt_name_singular, 'post type singular name' ),
        'add_new'            => _x( 'Adicionar novo(a)', $cpt_name_singular ),
        'add_new_item'       => __( 'Adicionar novo(a)' ),
        'edit_item'          => __( 'Editar '.$cpt_name_singular ),
        'new_item'           => __( 'Novo(a) '.$cpt_name_singular ),
        'all_items'          => __( 'Ver todos(as)' ),
        'view_item'          => __( 'Ver' ),
        'search_items'       => __( 'Procurar' ),
        'not_found'          => __( 'Nada encontrado' ),
        'not_found_in_trash' => __( 'Nada encontrado no lixo' ),
        'parent_item_colon'  => '',
        'menu_name'          => $cpt_name
    );
    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_nav_menus'     => true,
        'show_in_admin_bar'     => true,
        'query_var'             => true,
        'capability_type'       => 'post',
        'has_archive'           => true,
        'hierarchical'          => false,
        'can_export'            => true,
        'exclude_from_search'   => false,
                'taxonomies'            => array('post_tag'),
        'description'           => 'Adiciona entradas para '.$cpt_name,
        'rewrite'               => array('slug' => $cpt_slug),
        'menu_position'         => null,
        'supports'              => array( 'title','editor','thumbnail' ),
    );
    register_post_type( $cpt_slug, $args );
}
add_action( 'init', 'cpt_projects' );

/* EVENTOS CUSTOM POSTTYPE +++++++++++++++ */
function cpt_blog() {
    $cpt_name = 'Blog';
    $cpt_slug = 'blog';
    $cpt_name_singular = 'blog';
    $labels = array(
        'name'               => _x( $cpt_name, 'post type general name' ),
        'singular_name'      => _x( $cpt_name_singular, 'post type singular name' ),
        'add_new'            => _x( 'Adicionar novo(a)', $cpt_name_singular ),
        'add_new_item'       => __( 'Adicionar novo(a)' ),
        'edit_item'          => __( 'Editar '.$cpt_name_singular ),
        'new_item'           => __( 'Novo(a) '.$cpt_name_singular ),
        'all_items'          => __( 'Ver todos(as)' ),
        'view_item'          => __( 'Ver' ),
        'search_items'       => __( 'Procurar' ),
        'not_found'          => __( 'Nada encontrado' ),
        'not_found_in_trash' => __( 'Nada encontrado no lixo' ),
        'parent_item_colon'  => '',
        'menu_name'          => $cpt_name
    );
    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_nav_menus'     => true,
        'show_in_admin_bar'     => true,
        'query_var'             => true,
        'capability_type'       => 'post',
        'has_archive'           => true,
        'hierarchical'          => false,
        'can_export'            => true,
        'exclude_from_search'   => false,
		 		'taxonomies'            => array('post_tag'),
        'description'           => 'Adiciona entradas para '.$cpt_name,
        'rewrite'               => array('slug' => $cpt_slug),
        'menu_position'         => null,
        'supports'              => array( 'title','editor' ),
    );
    register_post_type( $cpt_slug, $args );
}
add_action( 'init', 'cpt_blog' );
