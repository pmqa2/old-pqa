<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pqa
 */

	$color = get_field('font_color');	
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('animated'); ?>>
	<header class="entry-header">
		<h2 class="entry-title"><?php the_title(); ?></h2>
	</header><!-- .entry-header -->

	<div class="entry-content intro">
		<div class="description"><?php the_content(); ?></div>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
