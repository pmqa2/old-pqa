<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content fullpage">

	<h1><?php the_title( ); ?></h1>

		<div class="inner-content">
			<?php the_content( ); ?>			
		</div>

	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php pqa_entry_footer(); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->