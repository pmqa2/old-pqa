<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pqa
 */

	$color = get_field('font_color');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h2 class="entry-title"><?php the_title(); ?></h2>
	</header><!-- .entry-header -->

	<div class="entry-content intro">
		<div class="intro-image"><?php echo get_the_post_thumbnail(); ?>
			<div class="social-icons">
				<a href="#"><i class="fi-social-dribbble"></i></a>
				<a href="https://github.com/pmqa"><i class="fi-social-github"></i></a>
				<a href="https://pt.linkedin.com/in/pedro-almeida-29970167"><i class="fi-social-linkedin"></i></a>
			</div>
		</div>
		<div class="intro-description" <?php if ($color) { echo 'style="color:'.$color.'"';  } ?>><?php the_content(); ?></div>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
